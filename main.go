package main
//based on https://www.loginradius.com/engineering/blog/google-authentication-with-golang-and-goth/

import (
	"fmt"
	"html/template"
	"net/http"
	
	"github.com/satori/go.uuid"
	"log"
	"time"

	"github.com/gorilla/pat"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
	"github.com/gorilla/sessions"
)
//1032837579249-9dmgah8abh5lmjst6o2uqqv04go8pie1.apps.googleusercontent.com
//2DFzb0a2EmLkgnNkqW0GEbyZ

func main() {
	
	key := "abc"  // Replace with your SESSION_SECRET or similar
	maxAge := 86400 * 30  // 30 days
	isProd := false       // Set to true when serving over https

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true   // HttpOnly should always be enabled
	store.Options.Secure = isProd

	gothic.Store = store

	goth.UseProviders(
		google.New("1032837579249-9dmgah8abh5lmjst6o2uqqv04go8pie1.apps.googleusercontent.com", "2DFzb0a2EmLkgnNkqW0GEbyZ", "http://localhost:3000/auth/google/callback", "email", "profile"),
	)

	p := pat.New()
	
	p.Get("/auth/{provider}/callback", func(res http.ResponseWriter, req *http.Request) {

		user, err := gothic.CompleteUserAuth(res, req)
		if err != nil {
			fmt.Fprintln(res, err)
			return
		}
		session, err := store.Get(req, "session")
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		id := uuid.NewV4()
		//log.Println(id)
		session.Values["uuid"] = id.String()
		session.Values["Name"] = user.Name
		session.Values["Email"]=user.Email
		
		
		err = session.Save(req, res)
		if err != nil {
            panic(err)// handle the error case
    	}
		t, _ := template.ParseFiles("templates/success.gohtml")
		t.Execute(res, user)
	})

	p.Get("/logout/{provider}", func(res http.ResponseWriter, req *http.Request) {
		gothic.Logout(res, req)
		session,_:=gothic.Store.Get(req,"session")
		session.Values["Email"]=""
		session.Values["Name"]=""
		session.Values["uuid"]=""
		session.Save(req, res)
		res.Header().Set("Location", "/")
		res.WriteHeader(http.StatusTemporaryRedirect)
	})

	p.Get("/auth/{provider}", func(res http.ResponseWriter, req *http.Request) {
		gothic.BeginAuthHandler(res, req)
	})

	p.Get("/", func(res http.ResponseWriter, req *http.Request) {
		session, err := store.Get(req, "session")
		log.Println("Session name: "+session.Values["uuid"].(string))
		t, _ := template.ParseFiles("templates/index.gohtml")
		res.Header().Set("Cache-Control", "max-age:10800, public")
		dbclient:=getClient()
		msgs:=getRecentMessages(dbclient,30)
		data:=struct{Name string;Msgs []message}{Name: "Anon", Msgs: msgs}
		if (session.Values["Name"] != nil && session.Values["Name"]!=""){
			data.Name=session.Values["Name"].(string)
		}
		err=t.ExecuteTemplate(res,"index.gohtml",data)
		if (err!=nil){ log.Println(err) }
		//t.Execute(res, false)
	})
	p.Post("/",func(res http.ResponseWriter, req *http.Request) {
		log.Println("posted")
		if err := req.ParseForm(); err != nil {
			// handle error
			panic(err)
		}
		log.Println(req.PostForm["message"])
		dbclient:=getClient()
		var msg message
		msg.Message=req.PostForm["message"][0]
		session, _ := store.Get(req, "session")
		if (session.Values["Name"].(string)==""){
			msg.Author="Anonymous"
		} else {
			msg.Author=session.Values["Name"].(string)
		}
		msg.Time=time.Now().Format("2006-01-02 15:04:05")
		addChatMessage(dbclient,msg)
		http.Redirect(res,req,"/",http.StatusSeeOther)
	})
	log.Println("listening on localhost:3000")
	log.Fatal(http.ListenAndServe(":3000", p))
}
