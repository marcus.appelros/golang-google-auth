module auth

go 1.13

require (
	github.com/gorilla/pat v1.0.1
	github.com/gorilla/sessions v1.1.1
	github.com/markbates/goth v1.64.0
	github.com/satori/go.uuid v1.2.0
	go.mongodb.org/mongo-driver v1.4.3
)
